//
//  UINavigationController+Rotation_IOS6.h
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Rotation_IOS6)

- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;

@end
