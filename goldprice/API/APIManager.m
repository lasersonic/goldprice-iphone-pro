//
//  APIManager.m
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "APIManager.h"
#import "SBJson.h"
#import "HistoryPrice.h"
#import <Security/Security.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation APIManager


-(MKNetworkOperation*) getGoldPriceOnCompletion:(Success) completion
                                        onError:(ErrorBlock) errorBlock{
    
    
    NSMutableDictionary *newParam = [self createParamsWithToken:nil];
    
    MKNetworkOperation *op = [self operationWithPath:@"getgoldprice.php"
                                              params:newParam
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        
        NSString *json = [completedOperation responseString];
        //NSLog(@"%@", json);
        SBJsonParser *parser = [[SBJsonParser alloc] init];
        NSDictionary *rootJson = [parser objectWithString:json];
        
        if(![self errorFromResponse:rootJson errorBlock:errorBlock]){
            
            NSDictionary *data = rootJson[@"data"];
            
            NSString *bid = [[data objectForKey:@"price"] objectForKey:@"bid"];
            NSString *ask = [[data objectForKey:@"price"] objectForKey:@"ask"];
            NSString *diff = [[data objectForKey:@"price"] objectForKey:@"diff"];
            NSString *time = [[data objectForKey:@"update"] objectForKey:@"ask"];
            NSString *timestamp = [[data objectForKey:@"update"] objectForKey:@"bid"];
            
            completion(bid, ask, diff, timestamp);
        }

    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) getHistoryPriceOnCompletion:(SuccessArray) completion
                                        onError:(ErrorBlock) errorBlock{
    
    
    NSMutableDictionary *newParam = [self createParamsWithToken:nil];
    
    MKNetworkOperation *op = [self operationWithPath:@"history.php"
                                              params:newParam
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSMutableArray *historyData = [NSMutableArray array];
        NSString *json = [completedOperation responseString];
        if([json length] < 5){
            completion(historyData);
        }else{
            //NSLog(@"%@", json);
            SBJsonParser *parser = [[SBJsonParser alloc] init];
            NSDictionary *rootJson = [parser objectWithString:json];
            
            NSArray *historyArray = [rootJson objectForKey:@"history"];
            
            for(NSDictionary *historyItem in historyArray){
                HistoryPrice *hp = [[HistoryPrice alloc] init];
                hp.buy = [historyItem objectForKey:@"buy"];
                hp.buy2 = [historyItem objectForKey:@"buy2"];
                hp.sell = [historyItem objectForKey:@"sell"];
                hp.sell2 = [historyItem objectForKey:@"sell2"];
                hp.datetime = [historyItem objectForKey:@"datetime"];
                hp.diff = [historyItem objectForKey:@"diff"];
                [historyData addObject:hp];
            }
            
            completion(historyData);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}


-(MKNetworkOperation*) getHistoryPriceOnCompletion:(NSString *) date
                                           success:(SuccessArray) completion
                                           onError:(ErrorBlock) errorBlock{
    
    NSDictionary *param =@{@"date": date};
    
    NSMutableDictionary *newParam = [self createParamsWithToken:param];
    
    MKNetworkOperation *op = [self operationWithPath:@"history_fromdate.php"
                                              params:newParam
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        NSMutableArray *historyData = [NSMutableArray array];
        NSString *json = [completedOperation responseString];
        if([json length] < 5){
            completion(historyData);
        }else{
            //NSLog(@"%@", json);
            SBJsonParser *parser = [[SBJsonParser alloc] init];
            NSDictionary *rootJson = [parser objectWithString:json];
            
            NSArray *historyArray = [rootJson objectForKey:@"history"];
            
            for(NSDictionary *historyItem in historyArray){
                HistoryPrice *hp = [[HistoryPrice alloc] init];
                hp.buy = [historyItem objectForKey:@"th_bid"];
                hp.sell = [historyItem objectForKey:@"th_ask"];
                hp.datetime = [historyItem objectForKey:@"datetime"];
                hp.diff = @"0";
                [historyData addObject:hp];
            }
            completion(historyData);
        }
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}



-(MKNetworkOperation*) insertUserToken:(NSString*)push_token
                              complete:(SuccessResponse) completion
                               onError:(ErrorBlock) errorBlock{
    
    NSDictionary *param = @{@"push_token": push_token
                            ,@"name" : [self getDeviceName]};
    
    NSMutableDictionary *newParam = [self createParamsWithToken:param];
    
    MKNetworkOperation *op = [self operationWithPath:@"addusertoken.php"
                                              params:newParam
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        completion([completedOperation responseString]);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) checkUpdateOnComplete:(SuccessResponse) completion
                               onError:(ErrorBlock) errorBlock{
    
    NSDictionary *param = @{@"version" : [self getAppVersion] };
    NSMutableDictionary *newParam = [self createParamsWithToken:param];
    
    MKNetworkOperation *op = [self operationWithPath:@"checkupdate.php"
                                              params:newParam
                                          httpMethod:@"GET"];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        completion([completedOperation responseString]);
    } onError:^(NSError *error) {
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(NSError*) errorFromResponse:(NSDictionary*) rootJson errorBlock:(ErrorBlock) errBlock{
    if (![rootJson[@"status"] boolValue]) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:rootJson[@"error_message"] forKey:NSLocalizedDescriptionKey];
        NSError *err = [[NSError alloc]initWithDomain:@"" code:[rootJson[@"error_code"] intValue] userInfo:details];
        errBlock( err );
        return err;
    }
    return  nil;
}

#pragma  mark - Get Device Info

-(NSString*) getDeviceName{
    
    NSString *name = [[UIDevice currentDevice] name];
    
    if (name == nil) {
        return @"Default";
    }
    
    return name;
}

-(NSString*) getAppVersion{
    return [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
}


#pragma mark - About Token

-(NSString*) generateNonce{
    int len = 10;
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddHHyyyymmMMss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    
    NSString *nonce = [randomString stringByAppendingString:dateString];
    
//    NSLog(@"nonce = %@", nonce);
    
    return nonce;
}

-(NSMutableDictionary*) createParamsWithToken:(NSDictionary*) dict{
    
    NSMutableDictionary *mutableDict = [[NSMutableDictionary alloc]init];
    
    if (dict) {
        mutableDict = [[NSMutableDictionary alloc]initWithDictionary:dict];
    }
    
    NSString *nonce = [self generateNonce];
    
    [mutableDict setObject:nonce forKey:@"nonce"];
    
    NSString *token = [self generateTokenFromRequest:mutableDict];
    
    [mutableDict setObject:token forKey:@"token"];
    
    return mutableDict;
}

-(NSString*) generateTokenFromRequest: (NSDictionary*) dict {
    
    
    NSArray * sortedKeys = [[dict allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    
    NSArray * objects = [dict objectsForKeys: sortedKeys notFoundMarker: [NSNull null]];
    
    NSString *token = @"";
    
    for (int i=0;i<sortedKeys.count;i++) {
        token = [[token stringByAppendingString:sortedKeys[i]] stringByAppendingString:objects[i]];
    }
    token = [token lowercaseString];
   
    token = [token stringByReplacingOccurrencesOfString: @" " withString: @""];
    
//    NSLog(@"data string = %@", token);
    
    token = [self hashed_string:token];
    
//    NSLog(@"token = %@", token);
    
    return token;
}

- (NSString *)hashed_string:(NSString *)data
{
//    NSLog(@"data => %@", data);
    
    NSString *key = @"Nut&Boo!";
    
    //enter your objects you want to encode in the data object
    NSData* secretData = [key dataUsingEncoding:NSUTF8StringEncoding];
    NSData* stringData = [data dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData *signatureData = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, secretData.bytes, secretData.length, stringData.bytes, stringData.length, signatureData.mutableBytes);

    NSString* token = [[[[signatureData description]
                         stringByReplacingOccurrencesOfString: @"<" withString: @""]
                        stringByReplacingOccurrencesOfString: @">" withString: @""]
                       stringByReplacingOccurrencesOfString: @" " withString: @""];

//    NSString *token = [signatureData base64EncodedStringWithOptions:0];
    
    return token;
}

- (NSString*) base64String:(NSString*) text{
    // Create NSData object
    NSData *nsdata = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [[NSString alloc]initWithData:[nsdata base64EncodedDataWithOptions:0] encoding:NSUTF8StringEncoding];
    
    return base64Encoded;
}

@end
