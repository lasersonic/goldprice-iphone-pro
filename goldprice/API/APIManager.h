//
//  APIManager.h
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface APIManager : MKNetworkEngine

typedef void (^Success)(NSString* bid, NSString *ask, NSString *diff, NSString *timestamp);
typedef void (^SuccessArray)(NSMutableArray *array);
typedef void (^ErrorBlock)(NSError* error);
typedef void (^SuccessResponse)(NSString* response);
typedef void (^SuccessGetListArray)(NSMutableArray *array, int totalpage);


-(MKNetworkOperation*) checkUpdateOnComplete:(SuccessResponse) completion
                                     onError:(ErrorBlock) errorBlock;

-(MKNetworkOperation*) getGoldPriceOnCompletion:(Success) completion
                               onError:(ErrorBlock) error;

-(MKNetworkOperation*) getHistoryPriceOnCompletion:(SuccessArray) completion
                                           onError:(ErrorBlock) errorBlock;

-(MKNetworkOperation*) insertUserToken:(NSString*)token
                              complete:(SuccessResponse) completion
                               onError:(ErrorBlock) errorBlock;

-(MKNetworkOperation*) getHistoryPriceOnCompletion:(NSString *) date
                                           success:(SuccessArray) completion
                                           onError:(ErrorBlock) errorBlock;

@end
