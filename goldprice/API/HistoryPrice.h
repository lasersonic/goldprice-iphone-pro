//
//  HistoryPrice.h
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryPrice : NSObject
@property (strong, nonatomic) NSString *buy, *buy2, *sell, *sell2, *diff, *datetime;
@end
