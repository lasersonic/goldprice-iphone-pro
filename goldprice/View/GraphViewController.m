//
//  GraphViewController.m
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "GraphViewController.h"
#import "ZoomImageViewController.h"

@interface GraphViewController (){

}

@end

@implementation GraphViewController


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //set right button
    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(loadImage)];
    [refresh setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem: refresh];
    [self.navigationItem setLeftBarButtonItem:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadImage];
}

-(void) loadImage{
    [self showLoading];
    [ApplicationDelegate.api cacheMemoryCost];
    NSURL *url = [NSURL URLWithString:@"http://www.kitco.com/images/live/gold.gif"];
    
    [ApplicationDelegate.api imageAtURL:url completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        [self.btnImgRealTime setImage:fetchedImage forState:UIControlStateNormal];
        [self hideLoading];
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        [self hideLoading];
    }];

    url = [NSURL URLWithString:@"http://www.kitco.com/LFgif/au0060lnb.gif"];
    
    [ApplicationDelegate.api imageAtURL:url completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        [self.btnImg60 setImage:fetchedImage forState:UIControlStateNormal];
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
    }];
    
    url = [NSURL URLWithString:@"http://www.kitco.com/LFgif/au0030lnb.gif"];

    [ApplicationDelegate.api imageAtURL:url completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        [self.btnImg30 setImage:fetchedImage forState:UIControlStateNormal];
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        
    }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)zoomRealtime:(id)sender{
    ZoomImageViewController * zoomView = [[ZoomImageViewController alloc]  initWithNibName:@"ZoomImageViewController" bundle:nil];
    zoomView.image = [[self.btnImgRealTime imageView] image];
    [self presentViewController:zoomView animated:YES completion:nil];
}

- (IBAction)zoom60Day:(id)sender {
    ZoomImageViewController * zoomView = [[ZoomImageViewController alloc]  initWithNibName:@"ZoomImageViewController" bundle:nil];
    zoomView.image = [[self.btnImg60 imageView] image];
    [self presentViewController:zoomView animated:YES completion:nil];
}

- (IBAction)zoom30Day:(id)sender {
    ZoomImageViewController * zoomView = [[ZoomImageViewController alloc]  initWithNibName:@"ZoomImageViewController" bundle:nil];
    zoomView.image = [[self.btnImg30 imageView] image];
    [self presentViewController:zoomView animated:YES completion:nil];
}

- (void)viewDidUnload {
    [self setBtnImg30:nil];
    [super viewDidUnload];
}

@end
