//
//  LatestPrice.m
//  goldpricePRO
//
//  Created by Nutthawut on 5/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "LatestPrice.h"

@implementation LatestPrice

LatestPrice *instance;
@synthesize bid, ask;

+(LatestPrice*)shareInstance{
    if(instance == nil){
        instance = [[LatestPrice alloc]init];
    }
    return instance;
}

@end
