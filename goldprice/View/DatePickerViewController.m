//
//  DatePickerViewController.m
//  goldpricePRO
//
//  Created by Nutthawut on 3/21/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeAction:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [outputFormatter setCalendar:calendar];
    [outputFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [outputFormatter stringFromDate:self.datePicker.date];
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(onCloseDatePickerView:)]) {
            [self.delegate onCloseDatePickerView:dateString];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{}
-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{

    UITouch *touch = [touches anyObject];
    if (touch.view == self.bgView){
        [self closeAction:nil];
    }
}

@end
