//
//  MyGoldViewController.m
//  goldpricePRO
//
//  Created by Nutthawut on 5/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "MyGoldViewController.h"
#import "LatestPrice.h"


@interface MyGoldViewController (){
    NSMutableArray *myGoldArrayPrice, *myGoldArrayAmount;
    UIAlertView *myAlertView;
    NSUserDefaults *pref;
    int profit;
}

@end

@implementation MyGoldViewController


-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    UIBarButtonItem *edit = [[UIBarButtonItem alloc] initWithTitle:@"แก้ไข" style:UIBarButtonItemStylePlain target:self action:@selector(editTable)];
    self.navigationItem.leftBarButtonItem = edit;
    
    UIBarButtonItem *insert = [[UIBarButtonItem alloc] initWithTitle:@"+Add" style:UIBarButtonItemStylePlain target:self action:@selector(showInputBox)];
    
    self.navigationItem.rightBarButtonItem = insert;
    
    [edit setTintColor:[UIColor whiteColor]];
    [insert setTintColor:[UIColor whiteColor]];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    pref = [NSUserDefaults standardUserDefaults];
    myGoldArrayPrice = [pref objectForKey:@"myGoldArrayPrice"];
    myGoldArrayAmount = [pref objectForKey:@"myGoldArrayAmount"];
    
    
    NSLog(@"%@", myGoldArrayPrice);
    
    if(myGoldArrayPrice==nil){
        myGoldArrayPrice = [NSMutableArray array];
    }
    if(myGoldArrayAmount==nil){
        myGoldArrayAmount = [NSMutableArray array];
    }
    

    [self calculateProfit];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showInputBox{
    myAlertView = [[UIAlertView alloc] initWithTitle:@"เพิ่มราคาทองของฉัน" message:@"" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"+Add",nil];
    
    [myAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    [[myAlertView textFieldAtIndex:0] setPlaceholder:@"ราคาทอง(บาท)"];
    [[myAlertView textFieldAtIndex:1] setPlaceholder:@"จำนวนที่ซื้อ (หน่วยเป็นบาท)"];
    [[myAlertView textFieldAtIndex:1] setSecureTextEntry:NO];
    [[myAlertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[myAlertView textFieldAtIndex:1] setKeyboardType:UIKeyboardTypeNumberPad];
    
    [myAlertView show];
}

-(void) calculateProfit{
    
    LatestPrice *lastPrice = [LatestPrice shareInstance];
    int overall = 0;
    int nowprice = 0;
    
       for(int i=0;i<[myGoldArrayAmount count];i++){
        overall += ([[myGoldArrayAmount objectAtIndex:i] intValue] * [[myGoldArrayPrice objectAtIndex:i] intValue]);
        nowprice += [lastPrice.bid intValue] * [[myGoldArrayAmount objectAtIndex:i]intValue];
    }
    
    profit = (nowprice - overall);

    NSLog(@"profit = %d %d", (nowprice - overall), nowprice);
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

-(void) insertRowWithPrice:(NSString*) price amount:(NSString*) amount{
    [myGoldArrayAmount addObject:amount];
    [myGoldArrayPrice addObject:price];
    [pref setObject:myGoldArrayAmount forKey:@"myGoldArrayAmount"];
    [pref setObject:myGoldArrayPrice forKey:@"myGoldArrayPrice"];
    [pref synchronize];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[myGoldArrayAmount count]-1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationLeft];
    
    [self calculateProfit];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    // Return the number of rows in the section.
    return [myGoldArrayAmount count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyGoldCell"];
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString * price = [NSString stringWithFormat:@"%@",[format stringFromNumber: [NSNumber numberWithInt:[[myGoldArrayPrice objectAtIndex:[indexPath row]] intValue]]]];
                        
    NSString * amount = [NSString stringWithFormat:@"%@",[format stringFromNumber: [NSNumber numberWithInt:[[myGoldArrayAmount objectAtIndex:[indexPath row]] intValue]]]];
    
    [((UILabel*)[cell viewWithTag:1]) setText: price];
    [((UILabel*)[cell viewWithTag:2]) setText: amount];
    
    return cell;
}

-(void) editTable{
    [self.tableView setEditing:!self.tableView.isEditing animated:YES];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSLog(@".. %d %d", [myGoldArrayAmount count],[indexPath row]);
        
        [myGoldArrayAmount removeObjectAtIndex:[indexPath row]];
        [myGoldArrayPrice removeObjectAtIndex:[indexPath row]];
        
        [pref setObject:myGoldArrayPrice forKey:@"myGoldArrayPrice"];
        [pref setObject:myGoldArrayAmount forKey:@"myGoldArrayAmount"];
        [pref synchronize];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self calculateProfit];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CGFloat width  = CGRectGetWidth(self.view.frame);
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0,0,width,40)];
    [header setBackgroundColor:[UIColor colorWithRed:1.0 green:0.3 blue:0.3 alpha:1]];
    UILabel *lbPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, width/2, 25)];
    [lbPrice setText:@"ราคาซื้อ(บาท)"];
    [lbPrice setTextColor:[UIColor whiteColor]];
    [lbPrice setBackgroundColor:[UIColor clearColor]];
    [lbPrice setTextAlignment:NSTextAlignmentCenter];
    [header addSubview:lbPrice];
    
    
    UILabel *lbNum = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 7, width/2, 25)];
    [lbNum setText:@"จำนวนทอง(บาท)"];
    [lbNum setTextColor:[UIColor whiteColor]];
    [lbNum setBackgroundColor:[UIColor clearColor]];
    [lbNum setTextAlignment:NSTextAlignmentCenter];
    [header addSubview:lbNum];
    
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    CGFloat width  = CGRectGetWidth(self.view.frame);
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0,0,width,40)];
    UILabel *lbPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, width/2, 25)];
    [lbPrice setText:@"กำไร(+)/ขาดทุน(-)"];
    [lbPrice setTextColor:[UIColor darkGrayColor]];
    [lbPrice setBackgroundColor:[UIColor clearColor]];
    [lbPrice setTextAlignment:NSTextAlignmentCenter];
    [footer addSubview:lbPrice];
    
    UILabel *lbTotalProfit = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 7, width/2, 25)];
    [lbTotalProfit setText:@"0"];
    [lbTotalProfit setFont:[UIFont systemFontOfSize:20]];
    [lbTotalProfit setTextColor:[UIColor darkGrayColor]];
    [lbTotalProfit setBackgroundColor:[UIColor clearColor]];
    [lbTotalProfit setTextAlignment:NSTextAlignmentCenter];
    [footer addSubview:lbTotalProfit];
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    
    if(profit> 0){
        [lbTotalProfit setText: [NSString stringWithFormat:@"+%@", [format stringFromNumber:[NSNumber numberWithDouble:profit]]] ];
        // [lbTotalProfit setText:@"nut"];
    }else{
        [lbTotalProfit setText: [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithDouble:profit]]] ];
    }
    
    return footer;
}

#pragma mark - AlertView delegate

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex==0){
        return;
    }
    if([[[alertView textFieldAtIndex:0] text] length] == 0
       || [[[alertView textFieldAtIndex:1] text] length] == 0){
        return;
    }
    
    [self insertRowWithPrice:[[alertView textFieldAtIndex:0] text] amount:[[alertView textFieldAtIndex:1] text] ];
}

@end
