//
//  BaseViewController.m
//  goldpricePRO
//
//  Created by Nutthawut on 3/21/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController (){
    BOOL isShowing;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showLoading{
    if (!isShowing){
        isShowing = YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

-(void) hideLoading{
    if (isShowing) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        isShowing = NO;
    }
}

-(void) showAlertDialogWihError:(NSError*)error{
    [self showAlertDialogWithTitle:@"" message:error.localizedDescription];
}

-(void) showAlertDialogWithTitle:(NSString*) title message:(NSString*) message{
    [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

@end
