//
//  LastPriceViewController.m
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "LastPriceViewController.h"
#import "LatestPrice.h"

@interface LastPriceViewController (){
}

@end

@implementation LastPriceViewController


-(void)viewWillAppear:(BOOL)animated{
    
    //set right button
    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refresh)];
    [refresh setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem: refresh];
    [self.navigationItem setLeftBarButtonItem:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self checkAppUpdate];
    
    [self refresh];
}

-(void) checkAppUpdate{
    [ApplicationDelegate.api checkUpdateOnComplete:^(NSString *response) {
        
    } onError:^(NSError *error) {
        [self showAlertDialogWihError:error];
    }];
}


-(void) refresh{
    [self showLoading];
    
    UILabel *headerBuy = (UILabel*)[self.view viewWithTag:10];
    UILabel *headerSell = (UILabel*)[self.view viewWithTag:20];
    UILabel *headerDiff = (UILabel*)[self.view viewWithTag:30];
    UILabel *lblBuy = (UILabel*)[self.view viewWithTag:11];
    UILabel *lblSell = (UILabel*)[self.view viewWithTag:22];
    UILabel *lblDiff = (UILabel*)[self.view viewWithTag:33];
    UILabel *lblLoadDate = (UILabel*)[self.view viewWithTag:55];
//    
//    [headerBuy setFont:[UIFont fontWithName:@"supermarket" size:30.0f]];
//    [headerSell setFont:[UIFont fontWithName:@"supermarket" size:30.0f]];
//    [headerDiff setFont:[UIFont fontWithName:@"supermarket" size:30.0f]];
//    [lblBuy setFont:[UIFont fontWithName:@"supermarket" size:70.0f]];
//    [lblSell setFont:[UIFont fontWithName:@"supermarket" size:70.0f]];
//    [lblDiff setFont:[UIFont fontWithName:@"supermarket" size:60.0f]];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [ApplicationDelegate.api getGoldPriceOnCompletion:^(NSString *bid, NSString *ask, NSString *diff, NSString* timestamp) {
        NSLog(@"getGoldPriceOnCompletion : %@ %@ %@ %@", bid, ask, diff, timestamp);
        
        [self storeDataForWatch:bid];
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval) [timestamp doubleValue]];
        NSLog(@"getGoldPriceOnCompletion Timestamp : %@", date);
        NSDateFormatter *formatterDate = [[NSDateFormatter alloc]init];
//        [formatterDate setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        //[formatterDate setDateStyle:NSDateFormatterMediumStyle];
        [formatterDate setDateFormat:@"ราคาอัพเดทเมื่อ dd/MM/yyyy HH:mm:ss"];
        
        LatestPrice *last = [LatestPrice shareInstance];
        last.bid = bid;
        last.ask = ask;
 
        [lblBuy setText: [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInt:[bid intValue]]]]];
        [lblSell setText:[NSString stringWithFormat:@"%@",[formatter stringFromNumber:[NSNumber numberWithInt:[ask intValue]]]]];
        [lblDiff setText:[NSString stringWithFormat:@"%@", [formatter stringFromNumber:[NSNumber numberWithInt:[diff intValue]]]]];
        [lblLoadDate setText:[formatterDate stringFromDate:date]];
        
        if([diff intValue] < 0){
            [self.imgChange setImage:[UIImage imageNamed:@"arrow_down"]];
        }else if([diff intValue] > 0){
            [self.imgChange setImage:[UIImage imageNamed:@"arrow_up"]];
        }
        
        [self hideLoading];
    } onError:^(NSError *error) {
        [self hideLoading];
        [self showAlertDialogWihError:error];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) storeDataForWatch:(NSString*) bidPrice
{
    NSUserDefaults *watchDefaults = [[NSUserDefaults alloc]initWithSuiteName:@"group.price"];
    [watchDefaults setObject:bidPrice forKey:@"bidPrice"];
}




@end
