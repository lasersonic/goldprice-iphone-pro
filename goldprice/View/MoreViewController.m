//
//  MoreViewController.m
//  goldprice
//
//  Created by Nutthawut on 4/28/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self setTitle:@"PRO VERSION"];
    [self.navigationItem setRightBarButtonItem:nil];
    [self.navigationItem setLeftBarButtonItem:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipeRight:)];
    [swipeGestureRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    [self.view addGestureRecognizer:swipeGestureRight];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadProAction:(id)sender {
    [[UIApplication sharedApplication]
     openURL:[NSURL URLWithString:@"https://itunes.apple.com/th/app/hk-clock/id629509551?l=en&mt=8"]];
}

@end
