//
//  GraphViewController.h
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)zoomRealtime:(id)sender;
- (IBAction)zoom60Day:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnImgRealTime;
@property (strong, nonatomic) IBOutlet UIButton *btnImg60;
@property (strong, nonatomic) IBOutlet UIButton *btnImg30;
- (IBAction)zoom30Day:(id)sender;

@end
