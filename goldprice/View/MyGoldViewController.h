//
//  MyGoldViewController.h
//  goldpricePRO
//
//  Created by Nutthawut on 5/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGoldViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
