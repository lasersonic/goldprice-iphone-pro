//
//  DatePickerViewController.h
//  goldpricePRO
//
//  Created by Nutthawut on 3/21/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerViewControllerDelegate <NSObject>
-(void) onCloseDatePickerView:(NSString*) dateSelected;
@end

@interface DatePickerViewController : UIViewController
@property (nonatomic, retain) id<DatePickerViewControllerDelegate> delegate;
- (IBAction)closeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
