//
//  BaseViewController.h
//  goldpricePRO
//
//  Created by Nutthawut on 3/21/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void) showLoading;
-(void) hideLoading;
-(void) showAlertDialogWihError:(NSError*)error;
-(void) showAlertDialogWithTitle:(NSString*) title message:(NSString*) message;


@end
