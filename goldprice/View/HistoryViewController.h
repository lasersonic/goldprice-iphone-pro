//
//  HistoryViewController.h
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatePickerViewController.h"

@interface HistoryViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, DatePickerViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)btnHistoryAction:(id)sender;

@end
