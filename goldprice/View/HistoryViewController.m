//
//  HistoryViewController.m
//  goldprice
//
//  Created by Nutthawut on 4/27/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryPrice.h"
#import "DatePickerViewController.h"

@class DatePickerViewController;

@interface HistoryViewController (){
    NSMutableArray *historyData;
    UIDatePicker *picker ;
}

@end

@implementation HistoryViewController


-(void)viewWillAppear:(BOOL)animated{
    
    //set right button
    UIBarButtonItem *refresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(getHistoryData)];
    [refresh setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem: refresh];
    [self.navigationItem setLeftBarButtonItem:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getHistoryData];
    
}

- (void)getHistoryData
{
    [self showLoading];
    
    [ApplicationDelegate.api getHistoryPriceOnCompletion:^(NSMutableArray *array) {
        
        if([array count] ==0){
            [((UILabel*)[self.view viewWithTag:101]) setAlpha:1];
        }
        [self hideLoading];
        historyData = array;
        
        [self.tableView reloadData];
        
    } onError:^(NSError *error) {
        [self hideLoading];
        [self showAlertDialogWihError:error];
    }];
}

-(void) generateMockupData{
    
    HistoryPrice *t = [[HistoryPrice alloc] init];
    t.datetime = @"19 April 2013 09.30";
    t.buy = @"21300";
    t.sell =@"20000";
    t.diff = @"-300";
    [historyData addObject:t];
    
    t = [[HistoryPrice alloc] init];
    t.datetime = @"19 April 2013 16.30";
    t.buy = @"21800";
    t.sell =@"21100";
    t.diff = @"900";
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
    [historyData addObject:t];
}

- (void)getHistoryDataWithDate:(NSString*) date
{
    [self showLoading];
    
    [ApplicationDelegate.api getHistoryPriceOnCompletion:date success:^(NSMutableArray *array) {
        
        if([array count] !=0){
            [((UILabel*)[self.view viewWithTag:101]) setAlpha:0];
        }else{
            [((UILabel*)[self.view viewWithTag:101]) setAlpha:1];
        }
        
        historyData = array;
        [self.tableView reloadData];
        
        [self hideLoading];
        
    } onError:^(NSError *error) {
        [self showAlertDialogWihError:error];
        [self hideLoading];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (historyData.count == indexPath.row) {
        return 60;
    }
    
    return 140;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [historyData count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row == [tableView numberOfRowsInSection:0]-1){
      
        cell = [tableView dequeueReusableCellWithIdentifier:@"InputDateCell"];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryCell"];
        [self setUpHistoryCell:cell indexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == historyData.count) {
        DatePickerViewController *dateView = (DatePickerViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
        dateView.delegate = self;
        [self presentViewController:dateView animated:YES completion:nil];
    }
}

-(void) setUpHistoryCell:(UITableViewCell*) cell indexPath:(NSIndexPath *) indexPath{
    
    UILabel *lblNum = (UILabel*)[cell viewWithTag:1];
    UILabel *lblDate = (UILabel*)[cell viewWithTag:2];
    UILabel *lblBuy = (UILabel*)[cell viewWithTag:3];
    UILabel *lblSell = (UILabel*)[cell viewWithTag:4];
    UILabel *lblDiff = (UILabel*)[cell viewWithTag:5];
    UIImageView *imgChange = (UIImageView*)[cell viewWithTag:99];
    
    //[lblNum setFont:[UIFont fontWithName:@"supermarket" size:22.0f]];
    //[lblDate setFont:[UIFont fontWithName:@"supermarket" size:18.0f]];
//    [lblBuy setFont:[UIFont fontWithName:@"supermarket" size:30.0f]];
//    [lblSell setFont:[UIFont fontWithName:@"supermarket" size:30.0f]];
//    [lblDiff setFont:[UIFont fontWithName:@"supermarket" size:25.0f]];
    
    
    HistoryPrice *hData = [historyData objectAtIndex:[indexPath row]];
    [lblNum setText: [NSString stringWithFormat:@"ครั้งที่ %u",[historyData count]- ([indexPath row])]];
    [lblDate setText: hData.datetime];
    [lblBuy setText: hData.buy];
    [lblSell setText: hData.sell];
    [lblDiff setText: hData.diff];
    
    if([hData.diff intValue] < 0){
        [lblDiff setTextColor:[UIColor redColor]];
        [imgChange setImage:[UIImage imageNamed:@"arrow_down"]];
    }else if([hData.diff intValue] > 0){
        [lblDiff setTextColor:[UIColor greenColor]];
        [imgChange setImage:[UIImage imageNamed:@"arrow_up"]];
    }else{
        [imgChange setAlpha:0];
        [lblDiff setTextColor:[UIColor lightGrayColor]];
    }
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"openDatePicker"]) {
        DatePickerViewController *datepickerViewController = (DatePickerViewController*)segue.destinationViewController;
        datepickerViewController.delegate = self;
        
    }
}

-(void) onCloseDatePickerView:(NSString *)dateSelected{
    [self getHistoryDataWithDate:dateSelected];
}

@end
