//
//  LatestPrice.h
//  goldpricePRO
//
//  Created by Nutthawut on 5/1/13.
//  Copyright (c) 2013 Nut Tang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class  LatestPrice;

@interface LatestPrice : NSObject

@property (nonatomic,strong) NSString *bid, *ask;

+(LatestPrice*)shareInstance;
@end
