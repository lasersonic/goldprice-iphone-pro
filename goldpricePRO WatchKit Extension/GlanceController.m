//
//  GlanceController.m
//  goldpricePRO WatchKit Extension
//
//  Created by Nutthawut on 8/11/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import "GlanceController.h"


@interface GlanceController()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblBuy;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblSale;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblChange;

@end


@implementation GlanceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    
    [self.lblBuy setText:@"-"];
    [self.lblSale setText:@"-"];
    [self.lblChange setText:@"Loading..."];
}


-(void) loadPriceFromIOSApp{
    
    NSDictionary *userInfo = @{@"askPrice" : [NSNumber numberWithBool:YES]};
    
    [WKInterfaceController openParentApplication:userInfo reply:^(NSDictionary *replyInfo, NSError *error) {
        if (error || !replyInfo) {
            NSLog(@"NutTang : Error open parentApplication");
            NSLog(@"%@", error);
            
            
            [self.lblBuy setText:@"-"];
            [self.lblSale setText:@"-"];
            [self.lblChange setText:@"-"];
        }
        else{
            //if no error , get user data & map to label
            
            NSString *bid   = [replyInfo objectForKey:@"bid"];
            NSString *ask   = [replyInfo objectForKey:@"ask"];
            NSString *diff  = [replyInfo objectForKey:@"diff"];
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            [self.lblBuy setText:[formatter stringFromNumber:[NSNumber numberWithInt:[bid intValue]]]];
            [self.lblSale setText:[formatter stringFromNumber:[NSNumber numberWithInt:[ask intValue]]]];
           
            if ([diff intValue] >= 0) {
                [self.lblChange setTextColor:[UIColor greenColor]];
                [self.lblChange setText: [NSString stringWithFormat:@"+%@", [formatter stringFromNumber:[NSNumber numberWithInt:[diff intValue]]]]];
            }else{
                [self.lblChange setTextColor:[UIColor redColor]];
                [self.lblChange setText: [NSString stringWithFormat:@"-%@", [formatter stringFromNumber:[NSNumber numberWithInt:[diff intValue]]]]];
            }
        }
    }];
}


- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    [self loadPriceFromIOSApp];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



