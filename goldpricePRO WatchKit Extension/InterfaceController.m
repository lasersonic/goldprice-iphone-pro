//
//  InterfaceController.m
//  goldpricePRO WatchKit Extension
//
//  Created by Nutthawut on 8/11/15.
//  Copyright (c) 2015 Nut Tang. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController(){
    int retry;
}
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblPriceBuy;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblPriceSale;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *lblPriceChange;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *imgChange;

@end

#define MAX_RETRY 3


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    
    [super willActivate];

    retry = 0;
    
    [self loadPriceFromIOSApp];

    
//    NSUserDefaults *watchDefaults = [[NSUserDefaults alloc]initWithSuiteName:@"group.price"];
//    NSString *bidPrice = [watchDefaults objectForKey:@"bidPrice"];
//    
//    if (bidPrice) {
//        [self.lblPriceBuy setText:bidPrice];
//    }
    
}

-(void) loadPriceFromIOSApp{
    
    if (retry > MAX_RETRY) {
        return;
    }
    
    NSDictionary *userInfo = @{@"askPrice" : [NSNumber numberWithBool:YES]};
    
    [WKInterfaceController openParentApplication:userInfo reply:^(NSDictionary *replyInfo, NSError *error) {
        if (error || !replyInfo) {
            NSLog(@"NutTang : Error open parentApplication");
            NSLog(@"%@", error);
            
            
            [self.lblPriceBuy setText:@"-"];
            [self.lblPriceSale setText:@"-"];
            [self.lblPriceChange setText:@"-"];
            
            retry++;
            
            [self loadPriceFromIOSApp];
        }
        else{
            //if no error , get user data & map to label
            
            NSString *bid   = [replyInfo objectForKey:@"bid"];
            NSString *ask   = [replyInfo objectForKey:@"ask"];
            NSString *diff  = [replyInfo objectForKey:@"diff"];
            
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            
            [self.lblPriceBuy setText:[formatter stringFromNumber:[NSNumber numberWithInt:[bid intValue]]]];
            [self.lblPriceSale setText:[formatter stringFromNumber:[NSNumber numberWithInt:[ask intValue]]]];
            [self.lblPriceChange setText:[formatter stringFromNumber:[NSNumber numberWithInt:[diff intValue]]]];
            
            if ([diff intValue] >= 0) {
                [self.imgChange setImage:[UIImage imageNamed:@"arrow_up"]];
                [self.lblPriceChange setTextColor:[UIColor greenColor]];
            }else{
                [self.imgChange setImage:[UIImage imageNamed:@"arrow_down"]];
                [self.lblPriceChange setTextColor:[UIColor redColor]];
            }
        }
    }];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



